package book.service;

import book.exception.DuplicateBookEntryException;
import book.exception.NonExistingBookException;
import book.model.Book;
import book.persistence.BookDao;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class BookService {
    private Logger logger = LoggerFactory.getLogger(BookService.class);

    @Inject
    private BookDao bookDao;

    @ConfigProperty(name = "pagination.maximum-record-per-page",defaultValue = "10")
    int maximumRecordPerPage;

    /***********************************************************************
     * To retrieve all the books recorded in the application
     * @param pageNumber the current page number
     * @return books the collection books which recorded in the application
     ***********************************************************************/
    public List<Book> retrieveAllBooks(int pageNumber) {
        logger.info("retrieveAllBooks");
        var startIndex = (pageNumber-1)*maximumRecordPerPage;
        var endIndex = pageNumber*maximumRecordPerPage;
        return bookDao.allBooks().all().subList(startIndex,endIndex);
    }

    /************************************************************************************************
     * To view the detail of existing Book based on the specific Book ID
     * @param bookId the referenced Book ID
     * @exception NonExistingBookException this exception will be thrown when the book was not exist
     ************************************************************************************************/
    public Book viewExistingBook(UUID bookId) throws NonExistingBookException {
        logger.info(String.format("viewExistingBook[bookId: %s]",bookId.toString()));
        return Optional.ofNullable(bookDao.findExistingBookById(bookId)).orElseThrow(()-> new NonExistingBookException(bookId));
    }


    /***************************************************************************************************************************************************
     * To record the new book
     * @param book the new book which needed to be recorded
     * @exception DuplicateBookEntryException this exception will be thrown when the book entry was considered as the data duplication based on the ISBN
     ***************************************************************************************************************************************************/
    public void newEntry(Book book) throws DuplicateBookEntryException {
        logger.info("newEntry");
        if(Optional.ofNullable(book).isPresent()) {
            if (anyBookUseThisISBN(book.getIsbn()))
                throw new DuplicateBookEntryException(book.getIsbn());
            else {
                book.setCreatedAt(Instant.now());
                bookDao.newBookEntry(book);
            }
        }
    }

    /***********************************************************************************************
     * To update the detail of existing book for the specific Book ID
     * @param bookId the specific Book ID need to be updated
     * @param modifiedBook the changes of the book details which came from the user input
     * @exception NonExistingBookException this exception will be thrown when the book was not exist
     ***********************************************************************************************/
    public void updateExistingBook(UUID bookId, Book modifiedBook) throws NonExistingBookException {
        logger.info(String.format("updateExistingBook[bookId: %s]",bookId.toString()));
        var updatedBook = Optional.ofNullable(bookDao.findExistingBookById(bookId)).map(book->book.mergeWith(modifiedBook)).orElseThrow(()-> new NonExistingBookException(bookId));
        bookDao.updateExistingBook(updatedBook);
    }

    /**********************************************************************************
     * To remove the existing book for the specific Book ID
     * @param bookId the specific Book ID which need to be removed
     * @exception NonExistingBookException throw this exception once it was not exist
     *********************************************************************************/
    public void removeExistingBook(UUID bookId) throws NonExistingBookException {
        logger.info(String.format("removeExistingBook[bookId: %s]",bookId.toString()));
        var book = Optional.ofNullable(bookDao.findExistingBookById(bookId)).orElseThrow(()-> new NonExistingBookException(bookId));
        bookDao.removeExistingBook(book);
    }

    /**********************************************************************
     * To check is there any books use that ISBN
     * @param isbn the input ISBN which need to check
     * @return true if any book use that ISBN. Otherwise, it returns false
     **********************************************************************/
    private boolean anyBookUseThisISBN(String isbn) {
        return bookDao.countingBookForThisBookRegistrationNumber(isbn)>0;
    }
}

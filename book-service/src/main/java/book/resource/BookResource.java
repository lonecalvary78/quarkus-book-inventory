package book.resource;

import book.exception.DuplicateBookEntryException;
import book.exception.NonExistingBookException;
import book.model.Book;
import book.service.BookService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.DefaultValue;

import java.util.List;
import java.util.UUID;

@Path("/books")
public class BookResource {
    @Inject
    BookService bookService;

    @GET
    public List<Book> retrieveAllBooks(@QueryParam("pageNum") @DefaultValue("1") int pageNumber) {
       return bookService.retrieveAllBooks(pageNumber);
    }

    @GET
    @Path("/{id}")
    public Book viewExisting(@PathParam("id") UUID bookId) throws NonExistingBookException {
        return bookService.viewExistingBook(bookId);
    }

    @POST
    public void newEntry(@Valid Book book) throws DuplicateBookEntryException {
        bookService.newEntry(book);
    }

    @PUT
    @Path("/{id}")
    public void updateExisting(@PathParam("id") UUID bookId, @Valid Book modifiedBook) throws NonExistingBookException {
       bookService.updateExistingBook(bookId,modifiedBook);
    }

    @DELETE
    @Path("/{id}")
    public void removeExisting(@PathParam("id") UUID bookId) throws NonExistingBookException {
        bookService.removeExistingBook(bookId);
    }
}

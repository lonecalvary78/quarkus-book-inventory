package book.exception;

public class DuplicateBookEntryException extends Exception {
    public DuplicateBookEntryException(String isbn) {
        super(String.format("Unable to process this book entry due to the data duplicate violation [ISBN: %s].",isbn));
    }
}

package book.exception;

import java.util.UUID;

public class NonExistingBookException extends Exception {
    public NonExistingBookException(UUID bookId) {
        super(String.format("Unable to find the book for this reference since it was not exist [Book ID: %s].",bookId.toString()));
    }
}

package book.exception.handler;

import book.exception.NonExistingBookException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NonExistingBookExceptionHandler implements ExceptionMapper<NonExistingBookException> {
    @Override
    public Response toResponse(NonExistingBookException nonExistingBookException) {
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}

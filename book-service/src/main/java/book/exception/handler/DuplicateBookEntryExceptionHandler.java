package book.exception.handler;

import book.exception.DuplicateBookEntryException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DuplicateBookEntryExceptionHandler implements ExceptionMapper<DuplicateBookEntryException> {
    @Override
    public Response toResponse(DuplicateBookEntryException duplicateBookEntryException) {
        return Response.status(Response.Status.CONFLICT).build();
    }
}

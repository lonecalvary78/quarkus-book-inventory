package book.persistence;

import com.datastax.oss.quarkus.runtime.api.session.QuarkusCqlSession;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Produces;

public class BookDaoProducer {
    private final BookDao bookDao;

    @Inject
    public BookDaoProducer(QuarkusCqlSession cqlSession) {
        var bookMapper = new BookMapperBuilder(cqlSession).build();
        this.bookDao = bookMapper.bookDao();
    }

    @Produces @ApplicationScoped
    public BookDao getBookDao() {
        return bookDao;
    }
}

package book.persistence;

import book.model.Book;
import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.mapper.annotations.*;

import java.util.UUID;

@Dao
public interface BookDao {
    @Select
    PagingIterable<Book> allBooks();

    @Select
    Book findExistingBookById(UUID bookId);

    @Insert
    void newBookEntry(Book book);

    @Update
    void updateExistingBook(Book book);

    @Delete
    void removeExistingBook(Book book);

    @Query("SELECT COUNT(1) FROM book WHERE isbn=:isbn")
    long countingBookForThisBookRegistrationNumber(String isbn);
}

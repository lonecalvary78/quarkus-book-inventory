# quarkus-book-inventory

This is sample book inventory application


## Getting started
### Configuration
It is necessary to configure the following db configuration:

- quarkus.cassandra.auth.username with the actual db username of your cassandra database on your environment
- quarkus.cassandra.auth.password with the actual db password of your cassandra database on your environment
- quarkus.cassandra.contact-points with the actual ip address of your cassandra database on your environment

### Build
You can the following command to build the application module

``mvn clean package``


